trigger ContactTrigger on Contact (after insert, after update,after delete) {
    if(trigger.isinsert && trigger.isafter)
        ContactTriggerHelper.afterInsert(trigger.new);
    if(trigger.isupdate && trigger.isafter)
        ContactTriggerHelper.afterUpdate(trigger.new);
    if(trigger.isdelete && trigger.isafter)
        ContactTriggerHelper.afterDelete(trigger.old);
}