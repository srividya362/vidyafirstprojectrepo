trigger AccountTrigger on Account (after insert,before insert,before update,after update,before delete) {
    if(trigger.isInsert && trigger.isafter){
        AccountTriggerHelper.AfterInsert (trigger.new);
    } 
    if(trigger.isInsert && trigger.isbefore){
        AccountTriggerHelper.BeforeInsert (trigger.new);
    }
    if(trigger.isUpdate && trigger.isbefore){
        AccountTriggerHelper.BeforeUpdate (trigger.new);
    }
    if(trigger.isUpdate && trigger.isafter){
        AccountTriggerHelper.AfterUpdate (trigger.new);
    } 
    
}