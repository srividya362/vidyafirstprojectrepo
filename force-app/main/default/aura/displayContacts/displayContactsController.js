({
	openRecord : function(component, event, helper){
        var x = event.currentTarget.id;
        var contacts = component.get("v.childContactsList");
        var contRec = contacts[x];
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": contRec.Id
        });
        navEvt.fire();
    },
    deleteRecord : function (compoenent,event,helper){
        var x = event.currentTarget.id;        
        var contacts = compoenent.get("v.childContactsList");        
        var contRec = contacts[x];        
        helper.deleteContact_Helper(compoenent,contRec);   
    }
})