({
	deleteContact_Helper : function(component, contRec) {
		var action = component.get("c.deleteContact_Apex");
        action.setParams({ deletedContact : contRec });
        action.setCallback(this, function(response) {
           
            var state = response.getState();
             alert(state);
            if (state === "SUCCESS") {
               component.set("v.childContactsList" ,response.getReturnValue() );
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	},
})