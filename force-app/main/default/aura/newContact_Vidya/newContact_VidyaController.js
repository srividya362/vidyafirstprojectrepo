({
	handleOnSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The record has been created successfully.",
            "type": "success",
            "mode": "pester"
        });
        toastEvent.fire();
        $A.get("e.force:closeQuickAction").fire(); 
        $A.get("e.force:refreshView").fire();   
             
    },
    afterAccountUpdate : function(cmp, event, helper){
        var accountId = cmp.get("v.recordId");
        alert('accountId'+accountId);
        console.log('--accountId--'+accountId);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The account record has been updated successfully.",
            "type": "success",
            "mode": "pester"
        });
        toastEvent.fire();
        $A.get("e.force:refreshView").fire(); 
    }
})