({
	handleComponentEvent : function(component, event, helper) {
		var message1 = event.getParam("msg"); 	
        component.set("v.messageFromChild" , message1);
	}
})