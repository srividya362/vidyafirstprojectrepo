({
	getContacts : function(cmp) {
		var action = cmp.get("c.getContacts_Apex");
        action.setParams({ accountId : cmp.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
              //  alert("From server: " + response.getReturnValue());
              //  alert("contact list: " + JSON.stringify(response.getReturnValue()));
                cmp.set("v.contactsList" ,response.getReturnValue() );
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	},
    
})