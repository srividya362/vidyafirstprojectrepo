public class ContactTriggerHelper{
    public static void afterInsert(list<Contact> conLIst){
        updateAccountLIst(conList);
    }
    public static void afterUpdate(list<Contact> conLIst){
        updateAccountLIst(conList);
    }
    public static void afterDelete(list<Contact> conLIst){
        updateAccountLIst(conList);
    }
    public static void updateAccountLIst(list<COntact> conList){
        set<Id> AccountIds = new set<ID>();
        list<account> acclIst = new list<Account>();
        for(Contact con: conLIst){
            if(con.AccountId != null)
                AccountIds.add(con.AccountId);    
        }
        for( Account acc : [select id,Amount__c, (select id,Amount__c from Contacts) from Account where id IN : AccountIds]){
            Decimal TotalAmount = 0;
            for(Contact con : acc.Contacts){    
                if(con.Amount__c != null)
                    TotalAmount += con.Amount__c ; // TotalAmount = TotalAmount + con.Amount__c ;        
            } 
            acc.Amount__c = TotalAmount ; 
            acclIst.add(acc);  
        }
        if(acclIst.size() > 0)
            update accList ;
    }
}