public class AccountBatch implements Database.batchable<SObject>{ 
   public Database.queryLocator start(Database.BatchableContext info){ 
       string query = 'Select id,name,accountnumber,type,NumberofLocations__c from Account' ;
       return Database.getQueryLocator(query);
   }     
   public void execute(Database.BatchableContext info, List<Account> scope){
       List<Account> accsToUpdate = new List<Account>();
       for(Account a : scope){ 
           a.Name = 'true'; 
           a.NumberOfEmployees = 70; 
           accsToUpdate.add(a); 
       } 
       update accsToUpdate; 
   }     
   public void finish(Database.BatchableContext info){ 
       system.debug('---finish--');    
   } 
}