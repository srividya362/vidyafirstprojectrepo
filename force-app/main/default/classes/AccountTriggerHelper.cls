public class AccountTriggerHelper{
    public static void AfterInsert( LIst<Account> accList){
       /* system.debug('----after insert method called--');
        list<Contact> conLIst = new List<COntact>();
        for(Account acc : accLIst) {
            Contact  con = new Contact();
            con.FirstName ='Sri';
            con.LastName = 'Vidya';   
            con.AccountId = acc.Id ;
            conLIst.add(con);
        }
        insert conLIst ;
        */
        list<Contact> conLIst = new List<COntact>();
        for(Account Acc: accLIst){
            if(acc.No_Of_Contacts__c != null && acc.No_Of_Contacts__c > 0){
                for(integer i=0;i<acc.No_Of_Contacts__c ; i++){
                    Contact  con = new Contact();
                    con.FirstName = acc.Name +' First';
                    con.LastName = acc.Name +' Last';   
                    con.AccountId = acc.Id ;
                    conLIst.add(con);   
                }
            }
        }
        if(conLIst.size() > 0)
        	insert conLIst ;
        
    }
    public static void BeforeInsert( LIst<Account> accList){
        updateSalesRep(accList);        
    }
    public static void BeforeUpdate( LIst<Account> accList){
        updateSalesRep(accList);
    }
    public static void AfterUpdate( LIst<Account> accList){
    }
    
    public static void updateSalesRep(LIst<Account> accList){
        trY{
            set<Id> userIds = new set<Id>();
            for(account acc : accList ){
                userIds.add(acc.OwnerId);
            }
            Map<Id,String> UserMap = new Map<Id,String>();
            for(User user1 : [select id,name from user where id IN : userIds]){
                UserMap.put(user1.Id, user1.Name);   
            }
            for(account acc : accLIst){
                acc.Sales_Rep__c = userMap.get(acc.ownerId);
            }
        }
        catch(Exception ex){
            system.debug('----message--'+ex.getMessage() );   
        }
    }
}