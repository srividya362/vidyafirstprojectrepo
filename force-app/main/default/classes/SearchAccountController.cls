public with sharing class SearchAccountController {
    public SearchAccountController() {

    }
    @AuraEnabled public static list<account> serachAccounts(string searchKey){
        return [select id,name,accountnumber from Account where Name Like : '%'+searchKey+'%' order by name limit 10];
    }
}