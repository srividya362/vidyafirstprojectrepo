global class Batch_AccountInsert implements Database.Batchable<sObject>,Database.RaisesPlatformEvents {
  //  global Set<Id> failureIdsSet;
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('select id from account limit 10');
    }
    global void execute(Database.BatchableContext bc, List<SObject> records){
        List<Account> AccountInsert = new list<Account>();
        list<string> failureIdsSet = new list<string>();
        for(integer i=0; i<10;i++){
            Account acc = new Account();
                if(Math.Mod(i,2) == 0)
                    acc.Name = 'Batch Insert--'+i ;
            AccountInsert.add(acc);
        }
      //  insert AccounTInsert ;
         Database.SaveResult[] srList = Database.insert(AccountInsert, false);
          // Iterate through each returned result 
          for (Database.SaveResult sr : srList) {
              system.debug('---'+sr.getId() );
             if (sr.isSuccess()) {
             }else{
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(sr+'---'+err.getStatusCode() + ': ' + err.getMessage());
                        failureIdsSet.add(err.getMessage());
                    }
             }
         }
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}