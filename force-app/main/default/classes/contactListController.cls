public class contactListController {
	@AuraEnabled
    public static list<Contact> getContacts_Apex(string accountId){
    	list<Contact> contactList = [select id,firstname,lastname,email,accountid from contact where accountid=: accountId];
        return contactList;
    }
    
    @AuraEnabled public static list<COntact> deleteContact_Apex(Contact deletedContact){
        string accId = deletedContact.accounTid ;
        delete  deletedContact ;
        list<Contact> conLIst = [select id,firstname,lastname,email from contact where accountid=: accId];
        system.debug('----contacts--'+conList);
        return conLIst;
    }
}