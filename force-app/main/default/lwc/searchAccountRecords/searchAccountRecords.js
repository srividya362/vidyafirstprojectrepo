import { LightningElement , track } from 'lwc';
import serachAccounts from '@salesforce/apex/SearchAccountController.serachAccounts'; 
    
export default class SearchAccountRecords extends LightningElement {
    @track accountList;
    @track error;
    handleSearchAccounts(event){
        const searchKey = event.target.value;
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            serachAccounts({ searchKey })
                .then(result => {
                    this.accountList = result;
                    this.error = undefined;
                })
                .catch(error => {
                    this.error = error;
                    this.accountList = undefined;
                });
        }, 350);
    }
}