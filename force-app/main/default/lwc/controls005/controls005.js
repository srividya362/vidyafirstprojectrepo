import { LightningElement } from 'lwc';

export default class Controls005 extends LightningElement {
    factors=[0,2,3,4,5];
    handleAdd(event){
        alert("I am in Handle Add");
    }
    handleSubtract(event){
        alert("I am in Handle Subtract");
    }
    handleMultiply(event){
        alert("I am in Handle Multiply");
    }
}